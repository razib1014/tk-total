function resultb() {
    var n, t, i, r, u, f, e, o, s, h, c = 0;
    n = 1e3 * Number(document.tkcalc.n1000.value);
    t = 500 * Number(document.tkcalc.n500.value);
    i = 200 * Number(document.tkcalc.n200.value);
    r = 100 * Number(document.tkcalc.n100.value);
    u = 50 * Number(document.tkcalc.n50.value);
    f = 20 * Number(document.tkcalc.n20.value);
    e = 10 * Number(document.tkcalc.n10.value);
    o = 5 * Number(document.tkcalc.n5.value);
    s = 2 * Number(document.tkcalc.n2.value);
    h = 1 * Number(document.tkcalc.n1.value);
    c = n + t + i + r + u + f + e + o + s + h;
    document.getElementById("st1000").innerHTML = n;
    document.getElementById("st500").innerHTML = t;
    document.getElementById("st200").innerHTML = i;
    document.getElementById("st100").innerHTML = r;
    document.getElementById("st50").innerHTML = u;
    document.getElementById("st20").innerHTML = f;
    document.getElementById("st10").innerHTML = e;
    document.getElementById("st5").innerHTML = o;
    document.getElementById("st2").innerHTML = s;
    document.getElementById("st1").innerHTML = h;
    document.getElementById("ttltkdisp").innerHTML = c + " TK"
}
